#
# wrapper around a python-gitlab group manager
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html
#
import logging

from rich import print

# from pprint import pprint

# from pydemi.cache import cached
from pydemi.gl_gitlab import connect_gitlab

logger = logging.getLogger(__name__)


# def get_cached(gl, _id):
#     @cached(f"group.{_id}.pickle")
#     def get_group(gl, _id):
#         group = gl.groups.get(_id)
#         return group

#     return get_group(gl, _id)


def print_summary(gr):
    print()
    print(f"{gr.name} ID: {gr.id}")
    print(f"{gr.full_name}")
    print()


def loop_on_projects(group):

    projects = group.projects.list(get_all=True)
    for project in projects:
        print(f"    {project.id} {project.name}")
        # proj.pprint()
        # print(project.to_json(sort_keys=True, indent=4))

    # headers = gl.projects.head()
    # print(f"{headers['X-Total']} objects of Content-Type: {headers['Content-Type']}")


def loop_on_groups(gl):

    headers = gl.groups.head()
    print(f"{headers['X-Total']} objects of Content-Type: {headers['Content-Type']}")

    # groups = gl.groups.list(iterator=True)
    groups = gl.groups.list(search="itk-demo-sw")
    for group in groups:
        gid = group.id
        gl.groups.get(gid)
        # pprint(group.attributes)
        # group.pprint()
        print(f"  {group.id} {group.name}")
        loop_on_projects(group)


def main():
    gl = connect_gitlab()


