""" Check files in repo
"""

import logging

from rich import print

logger = logging.getLogger(__name__)

required = [
    #### GitLab
    ".gitignore",
    ".gitlab-ci.yml",
    #### Docker
    ".dockerignore",
    "Dockerfile",
    # "LICENSE",
    "README.md",
    # "bin/run.sh",
    # "compile.sh",
    "config.sh",
    "docker-compose.yml",
    "openapi/openapi.yml",
    "pyproject.toml",
    # "pyslim.Dockerfile",
    # "pytest.py",
    "tasks.sh",
    "wsgi.py",
    #### Python project
    "{{project}}/__init__.py",
    "{{project}}/app.py",
    "{{project}}/backend.py",
    "{{project}}/routes.py",
    # tests
    # "tests/__init__.py",
    # "tests/parent_test.py",
    # "tests/test_api.py",
    #### UI
    # "ui/.eslintrc.json",
    # "ui/.npmrc",
    "ui/Dockerfile",
    "ui/docker-compose.yml",
    "ui/nginx.conf.template",
    "ui/package.json",
    "ui/public/index.html",
    # "ui/public/test.json",
    # "ui/public/test2.json",
    "ui/src/index.jsx",
    # "ui/src/screens/dashboard.jsx",
    "ui/src/style.css",
    "ui/tasks.sh",
]


def check_required(paths, pname, verbose=False):
    logger.info(f"Python project: {pname}")

    set_required = set([f.replace("{{project}}", pname) for f in required])
    set_present = set(paths)

    extras = set_present.difference(set_required)
    if verbose:
        print(f"Extra Files in {pname}:")
        print(sorted(list(extras)))

    missing = set_required.difference(set_present)
    print(f"Missing Files from {pname}:")
    print(sorted(list(missing)))

    return (len(extras), len(missing))
