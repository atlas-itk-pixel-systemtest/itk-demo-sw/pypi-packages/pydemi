import logging
import os
import subprocess
import sys
from pathlib import Path
from subprocess import CalledProcessError

import coloredlogs
import git
import yaml
from git import RemoteProgress
from git.exc import GitCommandError, InvalidGitRepositoryError, NoSuchPathError
from gitlab.exceptions import GitlabGetError
from rich import print as rprint
from rich.pretty import pprint
from tqdm import tqdm

from pydemi.gl_gitlab import connect_gitlab

from dotenv import load_dotenv

load_dotenv()

logger = logging.getLogger(__name__)
coloredlogs.install(
    level="INFO",
    #   fmt="[%(hostname)s] %(asctime)s %(name)s %(message)s",
    fmt="%(name)s %(message)s",
)

cwd = Path(os.getcwd())


class GitProgressPrinter(RemoteProgress):
    """progress printer from the GitPython tutorial"""

    def __init__(self):
        super().__init__()
        self.pbar = tqdm(unit="op")

    def update(self, op_code, cur_count, max_count=None, message=""):
        self.pbar.total = int(max_count)
        self.pbar.n = int(cur_count)
        if max_count:
            self.pbar.refresh()
        # logger.info(
        # print(
        #     f"{int(op_code):3d} {int(cur_count):6d} {int(max_count):6d} "
        #     f"{100.*(cur_count / (max_count or 100.0)):3.0f}% "
        #     f"{message}\r"
        # )


def parse_site_config(yamlfile="demi-site.yml"):
    with open(yamlfile) as yf:
        try:
            sc = yaml.safe_load(yf)
        except yaml.YAMLError as ye:
            logger.error(f"Could not load site config: {ye}")

    # logger.debug(sc)
    return sc


def find_repo_on_gitlab(slug, group_id=33370):
    """try to get URL from GitLab in configured group"""

    # logger.info(f"Looking for {slug}")
    gl = connect_gitlab()
    try:
        gr = gl.groups.get(group_id)
        # rprint(gr.attributes)
        # rprint(gr.to_json(sort_keys=True, indent=4))
        logger.debug(f"{slug} Working in GL group: {gr.name}")
    except GitlabGetError as ge:
        logger.error(f"{slug} Cannot find GitLab group id {group_id} : {ge}")
        return None

    path = gr.full_path + "/" + slug
    logger.debug(f"{slug} full_path_with_namespace : {path}")

    try:
        project = gl.projects.get(path)
    except GitlabGetError as ge:
        logger.error(f"{slug} Cannot find GitLab project {path} : {ge}")
        return None

    # project.pprint()

    # projects = gr.projects.list(search=f"{slug}", include_subgroups=True)
    # logger.info(f"{slug} search returns:")
    # for project in projects:
    #     print(f"{slug} ?  {project.id} {project.name}")
    #     rprint(pr.attributes)

    # if not projects:
    #     logger.error(f'{slug} No matching projects in group {gr.name}')
    #     for project in gr.projects.list(get_all=True, include_subgroups=True):
    #         print(f"{slug}?  {project.id} {project.path_with_namespace}")
    #     return None

    # if len(projects) > 1:
    #     logger.error(f"'{slug}' ambiguous. Could be:")
    #     for p in projects:
    #         print(p.name)
    #         project.pprint()
    #     return None

    # pr = projects[0]
    # rprint(pr.attributes)

    url = project.http_url_to_repo
    logger.info(f"{slug} Found Git repo URL: {url}")
    return url


# def clone_repo_from_gitlab():
# # create a local branch at the latest fetched master. We specify the name statically, but you have all
# # information to do it programmatically as well.
# bare_master = bare_repo.create_head("master", origin.refs.master)
# bare_repo.head.set_reference(bare_master)
# assert not bare_repo.delete_remote(origin).exists()


def load_repo(slug, dry_run=False):
    """Clone repo with name *slug* to path *dest*"""

    refdir = cwd / Path(".pydemi") / Path(slug)
    repo = None

    # Prefer local reference repo over remote clone from GitLab

    # logger.info(f"{slug}: Loading reference repo to {refdir}")
    try:
        repo = git.Repo(refdir)
        logger.info(f"{slug}: Local reference repo exists")

        # we have been cloned, so should be one remote
        # assertEqual(len(repo.remotes), 1)
        # assert repo.remotes.origin.exists()
        # repo.remotes.origin.pull()

        # origin = repo.remotes.origin
        # for fetch_info in origin.pull(progress=GitProgressPrinter()):
        #     print("Updated %s to %s" % (fetch_info.ref, fetch_info.commit))

        # if not update_repo(slug, repo):
        #     return None

        # if repo.is_dirty():
        #     logger.warning(f"{slug}: Local reference repo is dirty!")

        return repo

    except NoSuchPathError as ge:
        logger.warning(f"{slug} No such path {ge}")

    url = find_repo_on_gitlab(slug)
    if not url:
        logger.warning(f"{slug}: Cannot find {slug} repo.")
        return None

    if dry_run:
        return repo

    # Clone from GitLab

    try:
        logger.info(f"{slug}: Cloning reference repo into {refdir}")
        logger.debug(f"{slug}: from {url}")

        return git.Repo.clone_from(
            url=url,
            to_path=refdir,
            progress=GitProgressPrinter(),
            depth=1,
        )

    except GitCommandError as ge:
        logger.error(f"{slug}: Could not clone repo: {ge}")
        return None


def copy_repo(repo, slug, dest, dry_run=False):
    """Copy local repo"""
    path = dest
    logger.info(f"{slug}: Create a copy of the existing repo {slug} at {path}")
    if dry_run:
        return None
    try:
        rrepo = repo.clone(path)
        return rrepo
    except GitCommandError as ge:
        logger.error(f"{slug}: Could not clone repo: {ge}")

    return None


def run_config_script(slug, dest):
    """Run config script"""
    config_script = Path(dest) / "config"

    # print(config_script)
    if not config_script.is_file():
        config_script = Path(dest) / "config.sh"
        # print(config_script)
        if not config_script.is_file():
            logger.error(f"{slug}: No config script! Cannot configure {dest}")
            os.listdir(dest)
            return False
        logger.warning(f"{slug}: {config_script} does not follow naming convention!")

    if not os.access(config_script, os.X_OK):
        logger.error(f"{slug}: {config_script} not executable!")
        return False

    r = subprocess.run(
        config_script,
        capture_output=True,
        text=True,
        # shell=True,
        # stdout=subprocess.PIPE,
        # stderr=subprocess.PIPE,
        cwd=dest,
    )
    print(r.stdout)
    print(r.stderr)
    logger.info(f"{slug} configured.")
    return True


def update_repo(slug, repo):
    origin = repo.remotes.origin

    if not origin:
        logger.error(f"{slug}: No remote origin!")
        return False

    logger.info(f"{slug}: Update from {origin}")
    try:
        for fetch_info in origin.pull(progress=GitProgressPrinter()):
            logger.debug(f"{slug}: Updated {fetch_info.ref} to {fetch_info.commit}")
        return True
    except GitCommandError as ge:
        logger.error(f"{slug}: Could not update repo:\n{ge}")
        return False


def configure_stack(slug, dest):
    # slug = str(dest).split("/")[-1]
    # repo = None

    # try:
    #     repo = git.Repo(dest)
    # except InvalidGitRepositoryError as e:
    #     logger.warning(f"{slug}: Invalid Git repo: {e}")
    # except NoSuchPathError as e:
    #     logger.warning(f"{slug}: No such path: {e}")

    # if not repo:

    repo = load_repo(slug)
    # if not repo:
    #     return False
    # logger.debug(f"{slug}: {repo.description}")

    # depreceated - now write to instance folder
    # dest_repo = copy_repo(repo, slug, dest)

    if not update_repo(slug, repo):
        return False

    if repo.is_dirty():
        logger.warning(f"{slug}: Local reference repo is dirty!")
    else:
        logger.debug(f"{slug}: Local reference repo is clean.")

    return not run_config_script(slug, dest)


def docker_compose(
    slug, dest, compose_file="docker-compose.yml", cmd="convert", quiet=True
):
    """Run docker compose."""
    # slug = str(dest).split("/")[-1]
    cmd0 = f"docker compose --env-file .env -f {compose_file} {cmd}".split()
    # logger.info(f"Run {cmd}")
    try:
        r = subprocess.run(
            cmd0,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=dest,
            check=True,
        )
        if not quiet:
            print(r.stdout)
            print(r.stderr)
    except CalledProcessError as ce:
        logger.error(f"{slug} Failed to call docker compose")
        print(ce.stdout)
        print(ce.stderr)
        return False

    logger.info(f"{slug} docker compose {cmd} ok.")
    return True


def for_all_stacks(site, stacks, fn, *args, **kwargs):
    """Execute fn for all stacks in site."""
    rc = 0
    for stack in stacks:
        req = stacks[stack]["req"]
        slug = req.split("#")[0]
        # logger.info(f"{slug}")

        # TO DO: add site and instance
        dest = cwd / Path(".pydemi") / Path(slug)
        # logger.info(f"{slug} ─────── cmd")
        error_code = fn(slug, dest, *args, **kwargs)
        if error_code:
            rc += 1
            logger.error(f"{site}: Failed to run {fn.__name__} on {slug}: {error_code}")
            sys.exit(1)
    return rc


def main():
    sc = parse_site_config()
    rprint(sc)

    site = sc["site"]
    stacks = sc["stacks"]

    logger.info(f"{site} Configuring site")
    if not for_all_stacks(site, stacks, configure_stack):
        sys.exit(1)

    logger.info(f"{site} Converting docker compose files")
    if not for_all_stacks(site, stacks, docker_compose, cmd="convert"):
        sys.exit(1)

    # logger.info(f"{site} All stacks up")
    # for_all_stacks(site, stacks, docker_compose, cmd="up -d --build", quiet=False)

    logger.info(f"{site} All stacks down")
    for_all_stacks(site, stacks, docker_compose, cmd="down", quiet=False)

    # docker kill $(docker ps -q)
