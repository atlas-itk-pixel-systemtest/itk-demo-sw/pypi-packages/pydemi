"""object pickle decorator
taken from:
https://datascience.blog.wzb.eu/2016/08/12/a-tip-for-the-impatient-simple-caching-with-python-pickle-and-decorators/
"""

import os
import pickle
import logging
import pathlib

logger = logging.getLogger(__name__)


def cached(cachefile):
    """
    A function that creates a decorator which will use "cachefile"
    for caching the results of the decorated function "fn".
    """
    pathlib.Path(".cache").mkdir(parents=True, exist_ok=True)
    cachefile = f".cache/{cachefile}"

    def decorator(fn):  # define a decorator for a function "fn"
        def wrapped(
            *args, **kwargs
        ):  # define a wrapper that will finally call "fn" with all arguments
            # if cache exists -> load it and return its content
            if os.path.exists(cachefile):
                with open(cachefile, "rb") as cachehandle:
                    logger.debug(f"Using cached result from {cachefile}")
                    return pickle.load(cachehandle)

            # execute the function with all arguments passed
            res = fn(*args, **kwargs)

            # write to cache file
            with open(cachefile, "wb") as cachehandle:
                logger.debug(f"Saving to {cachefile}")
                pickle.dump(res, cachehandle)

            return res

        return wrapped

    return decorator
