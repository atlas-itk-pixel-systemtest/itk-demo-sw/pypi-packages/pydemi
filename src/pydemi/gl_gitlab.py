

import gitlab

import logging
import os

from pprint import pprint

logger = logging.getLogger(__name__)

GITLAB_URL = os.environ.get("GITLAB_URL", "https://gitlab.cern.ch")
GITLAB_PRIVATE_TOKEN = os.environ.get("GITLAB_PRIVATE_TOKEN")

def connect_gitlab():
    if not GITLAB_PRIVATE_TOKEN:    
        logger.debug("No GL_TOKEN env variable set - can only access public repos.")

    logger.debug(f"Connecting to {GITLAB_URL}")
    gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)

    # gl = gitlab.Gitlab.from_config('somewhere', ['/tmp/gl.cfg'])

    if GITLAB_PRIVATE_TOKEN:    
        try:
            gl.auth()
        except Exception as e:
            logger.warning("Can't authenticate.")

    # gl.enable_debug()

    # pprint(gl.__dict__)

    return gl
