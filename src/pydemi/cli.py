""" pydemi 

DeMi (Detector Microservices) Python CLI

2023, G. Brandt <gbrandt@cern.ch>
"""

import typer

from pydemi import pydemi
from pydemi import config_site_v0

app = typer.Typer(no_args_is_help=True)
group_app = typer.Typer()
projects_app = typer.Typer()
repo_app = typer.Typer()
site_app = typer.Typer()

app.add_typer(group_app, name="group", help="Work with GitLab group.")
app.add_typer(projects_app, name="projects", help="Work with GitLab project(s).")
app.add_typer(repo_app, name="repos", help="Work with Git repositories.")
app.add_typer(site_app, name="site", help="Work with your site.")

# @app.callback(invoke_without_command=True)
# def callback():
#     """show group info
#     """
#     pydemi.info_group()


@app.command()
def init(use_cache: bool = True, verbose: bool = False):
    """Initalize pydemi from CERN GitLab."""
    pydemi.init_projects()
    if verbose:
        pydemi.info_projects()


@group_app.command("info")
def group_info(
    # info_group: bool = typer.Option(True, "--group", "-g", help="Show group info."),
    # info_projects: bool = typer.Option(
    #     False, "--projects", "-p", #help="Show projects info."
    # ),
):
    """Check microservice group from CERN GitLab."""
    pydemi.info_group()


@projects_app.command("info")
def projects_info(
    # info_group: bool = typer.Option(False, "--group", "-g", help="Show group info."),
    # info_projects: bool = typer.Option(
    #     False, "--projects", "-p", #help="Show projects info."
    # ),
):
    """Check microservice repositories from CERN GitLab."""
    pydemi.info_projects()


@repo_app.command("clone")
def repos_clone():
    """Clone microservice repositories from CERN GitLab."""
    pydemi.clone_repos()


@repo_app.command("check")
def repos_check():
    """Check microservice repositories from CERN GitLab."""
    pydemi.check_repos()


# @repo_app.command("configure")
# def repos_configure():
#     """Configure microservices."""
#     pydemi.configure_repos()


@site_app.command("configure")
def site_configure():
    """Configure site."""
    config_site_v0.configure_site()
