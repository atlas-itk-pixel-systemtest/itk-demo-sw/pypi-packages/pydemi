# Site Configuration

import logging
import yaml
import chevron

import socket
from pathlib import Path
from pydemi.ls_tree import ls_tree

# from rich import print
from pprint import pprint

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def print_env(env):
    for k, v in env.items():
        print(f"{k}={v}")


class DemiSiteConfigurator:
    def __init__(self, sc):
        self.sc = sc
        self.site = sc.get("site")
        self.prefix = sc.get("prefix")
        logger.info(f"Configuring site: {self.site}")
        # set of GitLab projects
        self.projects = set()
        self.root = Path(".site-config")

        self.env = {}
        self.env["DEMI_SITE"] = f"{self.site}"

    def config_hosts(self):
        """Configure host objects"""
        hosts = self.sc.get("hosts")
        for host, values in hosts.items():
            hostname = values.get("hostname")
            host_ip = values.get("host_ip")
            logger.info(f"Configuring host: '{host}' on {hostname}")

            self.env["DEMI_HOST"] = host
            self.env["HOST"] = hostname
            self.env["HOST_IP"] = host_ip

            self.config_services(host, values)

    def config_services(self, host, hdict):
        """Configure services on one host oject"""
        # hostname = hdict.get("hostname")
        # print(f"HOSTNAME={hostname}")

        services = hdict.get("services")
        for service, values in services.items():
            logger.debug(service, values)

            self.env["DEMI_SERVICE"] = service

            # configure project

            project = values.get("project")
            logger.info(f"Configuring service instance: '{service}' ({project})")
            self.projects.add(project)

            # some default environment variables that can be defined automatically

            # configure environment variables from arguments
            args = values.get("args")
            if args:
                self.service_set_args(host, service, args)

            self.write_configuration(host, service)

    def service_set_args(self, host, service, args):
        logger.debug(
            f"Arguments for service instance {self.site}/{host}/{service}: {args}"
        )
        for arg in args:
            k, v = list(arg.items())[0]

            # Magic key expansion would go here ...

            # expand value - only
            # if not v.startswith(host)
            # if host not in v:
            #   v = f"{host}/{v}"
            # # if not v.startswith(self.site)
            # if self.site not in v:
            #   v = f"{self.site}/{v}"
            # if not v.startswith(prefix):
            # v = f"{prefix}/{self.site}/{v}"
            # keystr = f'{k.upper()}="{v}"'
            # print(f"{keystr}")
            self.env[k] = self.prefix + v

    def write_configuration(self, host, service):
        # collected all information, now write it

        logger.info(f"Environment for {host}/{service}:")
        print_env(self.env)

        # create directory structure to contain expanded configuration

        svcdir = Path(self.root / Path(host) / Path(service))
        svcdir.mkdir(parents=True, exist_ok=True)
        Path(svcdir / ".env").touch()
        Path(svcdir / "docker-compose.yml").touch()


def parse_site_config(yamlfile="site.template.yml"):
    """Parse site config, rendering the mustache template on the fly"""
    hostname = socket.gethostname()
    args = {
        "HOST": hostname,
        "HOST_IP": socket.gethostbyname(hostname),
        "SITE": hostname,
    }
    # print(args)

    with open(yamlfile, "r") as infile:
        # expand mustache template
        y = chevron.render(infile, args)
        # print(y)
        try:
            sc = yaml.safe_load(y)
        except yaml.YAMLError as e:
            logger.error(e)
            return

    logger.debug(sc)
    return sc


def configure_site():
    sc = parse_site_config()
    dsc = DemiSiteConfigurator(sc)
    dsc.config_hosts()

    # logger.info("Projects used:")
    # print(f"\n{dsc.projects}\n")
    # logger.info("Generated configuration files:")
    # print()
    # ls_tree(dsc.root)
