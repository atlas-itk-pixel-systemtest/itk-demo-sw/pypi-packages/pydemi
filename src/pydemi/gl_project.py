"""pydemi actions on a GitLab project
"""

import logging

import yaml

from pydemi.cache import cached
from gl_gitlab import connect_gitlab

logger = logging.getLogger(__name__)


def get_cached(gl, _id):
    @cached(f"project.{_id}.pickle")
    def get_project(gl, _id):
        proj = gl.projects.get(_id)
        return proj

    return get_project(gl, _id)


def print_summary(proj):
    logger.info(f"{proj.id} {proj.name}")
    # print(f"{proj.full_name}")
    logger.info(f"{proj.web_url}")


def check_ci(project):
    """fetch .gitlab-ci.yml,
    run the linter to get the full translated config,
    and extract the `image:` setting
    """
    logger.info("CI/CD image usage")

    logger.info(f"# Project: {project.name_with_namespace}, ID: {project.id}")

    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html#project-files - not needed but helpful for custom path search
    # data = project.files.raw(file_path='.gitlab-ci.yml', ref='main') # TODO: fetch default branch dynamically

    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/ci_lint.html
    lint_result = project.ci_lint.get()
    if not lint_result.merged_yaml:
        logger.info("couldn't get lint CI yaml")
        return

    # logger.info(lint_result.merged_yaml)  # this is yaml

    data = yaml.safe_load(lint_result.merged_yaml)
    # logger.info(data) #debug

    for d in data:
        # logger.info(d) #debug
        logger.info(f"Job name: {d}")
        for attr in data[d]:
            if "image" in attr:
                logger.info(f"Image: {data[d][attr]}")

    # Loop through all jobs (this can be expensive, and is not working well yet. Remove the continue statement below to play).
    return

    for job in project.jobs.list():
        log_trace = str(job.trace())

        logger.info(log_trace)

        if "image" in log_trace:
            logger.info("Job ID: {i}, URL {u}".format(i=job.id, u=job.web_url))
            logger.info(log_trace)

def main():
    gl = connect_gitlab()

    projects = gl.projects.list()

