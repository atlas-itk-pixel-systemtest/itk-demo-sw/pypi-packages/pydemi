import os
import subprocess

# from pprint import pprint
from rich import print
from pydemi import gl_project
from pydemi.pydemi import connect_gitlab

# from tempfile import TemporaryDirectory


def hl(text):
    term_size = os.get_terminal_size()
    print("\u2500" * term_size.columns)
    print(text)
    print("\u2500" * term_size.columns)


def read_file(project):

    # print(files)

    ref = "main"

    # config = project.files.get("config.sh", ref=ref).decode().decode()

    # config = project.files.raw("config.sh", ref=ref)
    # print(config.decode())

    # raw_content = project.files.raw(file_path='README.rst', ref='main')
    # print(raw_content)
    with open("/tmp/config.sh", "wb") as f:
        project.files.raw(file_path="config.sh", ref=ref, streamed=True, action=f.write)

    with open("/tmp/docker-compose.yml", "wb") as f:
        project.files.raw(
            file_path="docker-compose.yml", ref=ref, streamed=True, action=f.write
        )

    with open("/tmp/ui.docker-compose.yml", "wb") as f:
        project.files.raw(
            file_path="ui/docker-compose.yml", ref=ref, streamed=True, action=f.write
        )

    # dc_ui = project.files.get("ui/docker-compose.yml", ref=ref).decode().decode()

    # print(config)
    # with open("_config.sh", "w+") as f:
    #     f.writelines(config)

    # with open("docker-compose.yml", "w+") as f:
    #     f.writelines(dc_api)

    # make file executable

    # import os
    # import stat
    # st = os.stat('somefile')
    # os.chmod('somefile', st.st_mode | stat.S_IEXEC)
    bash(["/tmp/config.sh"])


def bash(cmd):
    hl(["bash"] + cmd)
    r = subprocess.run(
        ["bash"] + cmd,
        text=True,
        # shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        # cwd=path
    )
    print(r.stdout)

    docker_compose_config("/tmp/docker-compose.yml")
    docker_compose_config("/tmp/ui.docker-compose.yml")


def main():
    gl = connect_gitlab()

    PROJECT_ID = 137084  # itk-demo-felix

    project = gl_project.get_cached(gl, PROJECT_ID)

    # project.pprint()
    #  project.repository_tree()
    # files = [f['name'] for f in project.repository_tree()]

    # pt = project.repository_tree(
    #     recursive=True,
    #     get_all=True,
    #     ref="main",
    # )

    # print(pt)
    # files = [(f["type"], f["path"]) for f in pt]

    read_file(project)


if __name__ == "__main__":
    main()
