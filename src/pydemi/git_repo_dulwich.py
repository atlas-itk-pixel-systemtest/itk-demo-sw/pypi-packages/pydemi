"""pydemi actions on a Git repo (using Dulwich)
https://www.dulwich.io/
https://git-scm.com/book/en/v2/Appendix-B%3A-Embedding-Git-in-your-Applications-Dulwich
"""
from os import environ
from pathlib import Path

# from tempfile import TemporaryDirectory

from dulwich import porcelain
from dulwich.repo import Repo
from urllib3 import ProxyManager

import logging

# from pydemi.cache import cached

logger = logging.getLogger(__name__)

## Configuration settings:
# Source url of the repo (Note: https here. ssh would work as well, a bit differently).
# GITURL = "https://github.com/lomignet/thisdataguy_snippets"
# Gihthub token: https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token
# TOKEN = "12345blah"
## /end of configuration.


def get(target):
    gitdir = Path(".repos") / Path(target)
    try:
        repo = Repo(gitdir)
        return repo
    except Exception as e:
        logger.error(f"Cannot find repo {target} {e}")
    return None


def clone(url, target, token=""):
    # If the environment variable https_proxy exists,
    # we need to tell Dulwich to use a proxy.
    # if environ.get("https_proxy", None):
    #     pool_manager = ProxyManager(environ["https_proxy"], num_pools=1)
    # else:
    #     pool_manager = None

    # with TemporaryDirectory() as gitrootdir:
    # with '.repos' as gitrootdir:

    repo = get(target)
    if repo:
        logger.info(f"{target} exists already, not cloning.")
        return repo

    gitdir = Path(".repos") / Path(target)
    gitdir.mkdir(parents=True, exist_ok=True)
    logger.info(f"Cloning '{url}' to '{gitdir}'")
    repo = porcelain.clone(
        url,
        gitdir,
        checkout=True,
        depth=1,
        # pool_manager=pool_manager,
    )
    logger.info("Cloned.")

    # print(repo)
    # print(repo.get_description())
    # porcelain.ls_tree(repo)
    return repo


def work_on_repo():
    # Do something clever with the files in the repo, for instance create an empty readme.
    readme = gitdir / "readme.md"
    readme.touch()
    porcelain.add(repo, readme)

    logger.info("Committing...")
    porcelain.commit(repo, "Empty readme added.")
    logger.info("Commited.")


def push(repo):
    logger.info("Pushing...")
    porcelain.push(
        repo,
        remote_location=GITURL,
        refspecs="master",  # branch to push to
        password=TOKEN,
        # Tokens are kinda public keys, no need for a username but it still needs to be provided for Dulwich.
        username="not relevant",
        pool_manager=pool_manager,
    )
    # Note: Dulwich 0.20.5 raised an exception here. It could be ignored but it was dirty:
    # File ".../venv/lib/python3.7/site-packages/dulwich/porcelain.py", line 996, in push
    #     (ref, error.encode(err_encoding)))
    # AttributeError: 'NoneType' object has no attribute 'encode'
    # Dulwich 0.20.6 fixed it.
    logger.info("Pushed.")
