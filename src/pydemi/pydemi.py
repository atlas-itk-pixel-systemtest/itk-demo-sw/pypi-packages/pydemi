#!/usr/bin/env python

# import json
import logging
import os
import subprocess
import sys
from itertools import repeat
from os import environ
from pathlib import Path
from re import sub

import coloredlogs
import gitlab
import rich

# from python_on_whales import DockerClient
from rich.console import Console
from rich.pretty import pprint
from rich.table import Table

from pydemi import check_files, git_repo_dulwich, gl_group, gl_project
from pydemi.gl_gitlab import connect_gitlab

# from dulwich import porcelain


# logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
coloredlogs.install(
    level="WARNING",
    fmt="[%(hostname)s] %(asctime)s %(name)s %(message)s",
)

PROJECT_ID = environ.get("GL_PROJECT_ID")
# https://gitlab.com/gitlab-de/use-cases/docker
# itk-demo-sw 33370
GROUP_ID = environ.get("GL_GROUP_ID", 33370)

project_filter = [
    "itk-demo-felix",
    "itk-demo-configdb",
    "itk-demo-template",
    "itk-demo-daqapi",
    "itk-demo-optoboard",
    "itk-demo-resultviewer",
    "itk-demo-dashboard",
    # "GALE",
]


def snake_case(s):
    return "_".join(
        sub(
            "([A-Z][a-z]+)", r" \1", sub("([A-Z]+)", r" \1", s.replace("-", " "))
        ).split()
    ).lower()




# Collect all projects, or prefer projects from a group id, or a project id


# if __name__ == "__main__":
def init_projects():
    gl = connect_gitlab()
    projects = []

    # Direct project ID
    if PROJECT_ID:
        logger.info(f"Getting project {PROJECT_ID}")

        project = gl_project.get_cached(gl, PROJECT_ID)
        projects.append(project)

    # Groups and projects inside
    elif GROUP_ID:
        logger.info(f"Getting projects from group {GROUP_ID}")

        group = gl_group.get_cached(gl, GROUP_ID)
        gl_group.print_summary(group)

        for project in group.projects.list(include_subgroups=True, all=True):
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            if project.name not in project_filter:
                logger.debug(f"Skipping {project.id} {project.name}")
                continue
            logger.info(f"Getting project {project.id} {project.name}")
            manageable_project = gl_project.get_cached(gl, project.id)
            # manageable_project = gl.projects.get(project.id)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        logger.info(f"Getting *all* projects from {GITLAB_SERVER}")
        projects = gl.projects.list(get_all=True)

    # logger.info(f"Work on list of projects {project_filter}:")

    # gl_project.check_ci(project)

    return projects


def info_group():
    gl = connect_gitlab()
    group = gl_group.get_cached(gl, GROUP_ID)
    gl_group.print_summary(group)


def info_projects():
    """Check repos."""
    projects = init_projects()
    for project in projects:
        # pprint(project.to_json(),indent_guides=True,expand_all=True)
        # pprint(json.loads(project.to_json()))

        items = project.repository_tree(get_all=True)
        pprint(items)


def clone_repos():
    """Clone repos."""
    projects = init_projects()
    for project in projects:
        gl_project.print_summary(project)
        git_repo_dulwich.clone(project.web_url + ".git", project.name)


def check_repos():
    """Check repos."""
    projects = init_projects()
    table = Table("Microservice", "Total", "Extras", "Missing")
    for project in projects:
        # get all files in repo
        repo = git_repo_dulwich.get(project.name)
        if not repo:
            continue
        paths = []
        store = repo.object_store
        tree_id = repo[b"HEAD"].tree
        for entry in store.iter_tree_contents(tree_id):
            # path = entry.in_path(bytes(repo.path)).path
            # print(entry.path.decode()) #, path.decode())
            paths.append(entry.path.decode())

        (extras, missing) = check_files.check_required(paths, snake_case(project.name))

        table.add_row(
            project.name, str(len(paths)), str(extras), "[red]" + str(missing)
        )

    Console().print(table)


def configure_repos():
    """Generate .env files for repos"""
    projects = init_projects()
    # paths = []
    compose_files = []
    for project in projects:
        repo = git_repo_dulwich.get(project.name)
        logger.debug(repo.path)

        def _configure(path, config_script, postfix="", verbose=False):
            logger.info(f"Configuring {path}")

            try:
                c = subprocess.run(
                    config_script,
                    capture_output=True,
                    cwd=path,
                )
            except Exception as e:
                logger.error(e)
                return
            if verbose:
                rich.print(c.stdout.decode())

            try:
                # docker = DockerClient(compose_files=[path / Path("docker-compose.yml")])
                # r = docker.compose.config(return_json=True)

                compose_file = f"docker-compose.{project.name}{postfix}.yml"
                with open(compose_file, "w") as of:
                    r = subprocess.run(
                        ["docker", "compose", "config"], text=True, stdout=of, cwd=path
                    )
                    # print(r.stdout)

            except Exception as e:
                logger.error(e)
                return

            if verbose:
                pprint(r)

            compose_files.append(compose_file)

        abspath = repo.path.resolve()
        _configure(abspath, "./config.sh")
        _configure(abspath / Path("ui"), "../config.sh", "-ui")  # ,verbose=True)

    logger.info("Configured microservices:")
    rich.print(compose_files)
    # docker = DockerClient(
    #     compose_files=[path / Path("docker-compose.yml") for path in paths]
    # )
    # r = docker.compose.config(return_json=True)
    # pprint(r)

    cmd = (
        ["docker", "compose"]
        + [c for pair in zip(repeat("-f"), compose_files) for c in pair]
        + ["config"]
    )
    print(cmd)
    merge_compose_files(cmd)


def merge_compose_files(cmd):
    group_name = "itk-demo-sw"
    compose_file = f"docker-compose.{group_name}.yml"
    with open(compose_file, "w") as of:
        r = subprocess.run(
            cmd,
            text=True,
            stdout=of,
            # cwd=path
        )
