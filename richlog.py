from rich.logging import RichHandler
from rich.progress import Progress, TaskID

logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(show_path=False)],
)

loop = asyncio.get_event_loop()
reader = asyncio.StreamReader(loop=loop)
protocol_factory = functools.partial(MyProtocol, reader, limit=2**16, loop=loop)


async def log_stderr(progress: Progress, taskid: TaskID) -> None:
    async for line in reader:
        m = re.search(r"\((\d+)%\)", line.decode())
        if m:
            p = int(m.group(1))
            progress.update(taskid, advance=p)
        else:
            logger.debug("%s", line.decode().rstrip())


transport, protocol = await loop.subprocess_exec(
    protocol_factory,
    *cmd,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
)

proc = asyncio.subprocess.Process(transport, protocol, loop)

with Progress() as progress:
    taskid = progress.add_task(str(cmd[0]), total=100)
    (out, err), _ = await asyncio.gather(
        proc.communicate(), log_stderr(progress, taskid)
    )
