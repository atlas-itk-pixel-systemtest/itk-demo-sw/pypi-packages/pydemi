#!/usr/bin/env bash

cmd=config

pwd
cwd=$(pwd)

GL_SERVER=${GL_SERVER:-gitlab.cern.ch}
GL_PROJECTID=137084
GL_GROUPID=33370

DEMI_SITE=${DEMI_SITE:-$(hostname | xargs)}

urlencode() {
    # urlencode <string>

    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for ((i = 0; i < length; i++)); do
        local c="${1:$i:1}"
        case $c in
        [a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
        *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

urldecode() {
    # urldecode <string>

    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

say() {
    printf '\e[0;32m┌─ %s\n\e[0;32m└' "$1"
    printf '%.s─' $(seq 1 "$(($(tput cols) - 1))") || true
    printf '\e[0m'
}

get_file() {
    local uearg=$(urlencode "${1}")
    local url="https://${GL_SERVER}/api/v4/projects/${GL_PROJECTID}/repository/files/${uearg}?ref=main"
    say "${url}"
    local content
    content=$(curl -sSL "${url}" | jq -r '.content' | base64 -d) || {
        echo "[✗] $1 not found."
        return 1
    }
    echo "[✓] $1 ok (${#content} bytes)."
    return 1
}

stack() {
    say "${1}"
    # repo=$1
    # mkdir -p "${DEMI_SITE}/${repo}"
    # cd "${DEMI_SITE}/${repo}" || exit
    get_file config
    get_file docker-compose.yml
    # chmod a+x config.sh
    # ./config.sh && docker compose "${cmd}"
    # mkdir -p ui
    # cd ui || exit
    get_file ui/config
    get_file ui/docker-compose.yml
    # ../config.sh && docker compose "${cmd}" &> /dev/null
}

stack itk-demo-felix

cd "${cwd}" || exit
pwd
tree ${DEMI_SITE}
