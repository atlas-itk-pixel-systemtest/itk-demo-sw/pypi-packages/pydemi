# https://stackoverflow.com/questions/75232761/how-do-i-run-poetry-install-in-a-subprocess-without-reference-to-the-current-vir

import os
import subprocess

other_directory = "/some/path"
current_env = os.environ.copy()

if "VIRTUAL_ENV" in current_env:
    del current_env["VIRTUAL_ENV"]

subprocess.run(
    [f"cd {other_directory} && poetry install && poetry env info"],
    cwd=other_directory,
    shell=True,
    check=True,
    env=current_env,
)
