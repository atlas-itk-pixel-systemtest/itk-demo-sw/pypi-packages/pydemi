import requirements
from rich import inspect, print

# from rich.color import Color
# color = Color.parse("red")
# inspect(color, methods=True)

with open("requirements.txt", "r") as fd:
    for req in requirements.parse(fd):
        print(req.name, req.specs)
        # inspect(req, methods=True)
