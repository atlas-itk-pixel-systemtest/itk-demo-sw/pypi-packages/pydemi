# pydemi

[[_TOC_]]

Command Line Tool to work with DeMi (detector microservices) for one site. It's main purpose is to manage the externalized configuration of the microservices.

Contact: Gerhard Brandt <gbrandt@cern.ch>

Planned minimal functionality:

- Configure your site for DeMi:

  - Define your abstract site topology in a YAML template.
  - Inject site-specific data to generate expanded site definition.
  - Validate against JSON schema (optional).
  - (also optional: write directly your YAML site definition by hand).
  - Run `pydemi` to define environment variables for each microservice instance by generating `.env` files by calling `config.sh` for each repository.
  - Generate expanded canonical `docker compose` files for all your hosts and microservice stacks in a directory tree corresponding to the host/service/container hierarchy defined in your site definition.

- Operate your DeMi site:
  - Loop on the hosts to set the Docker context.
    - Loop on the `docker compose` files to bring up all compose projects (container stacks).
  - Open your browser and control your site.

Other functionality:

- Validate microservice repositories.
  - Check for missing and extra files.
  - Later: format, lint, semantics, etc. etc.

## Installation

### Using poetry

From the GitLab PyPI registry:

```shell
poetry source add pydemi https://gitlab.cern.ch/api/v4/projects/155779/packages/pypi/simple
poetry add pydemi
```

Local installation (for development):

```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/pydemi.git
cd pydemi
poetry install
poetry shell
```

### Using pip

From the GitLab PyPI registry:

```shell
pip install pydemi --extra-index-url https://gitlab.cern.ch/api/v4/projects/155779/packages/pypi/simple
```

Directly from the Git repo:

```shell
pip install git+https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/pydemi.git
```

## Configuring your site


Manually edit `demi-site.yml`

This is the only supported option right now.

<!--
###   Generate Configuration

To configure your site, edit `demi-site.data.json` (mustache template data).
Then run `expand.sh` to:

1. expand the site template variables.
1. validate against the DeMi Site specification JSON schema.

This creates `demi-site.yml`.

`demi-site.data.json` and `demi-site.yml` are local site files and _should not be checked into Git_! (only into a deployment specific private repo, if
you have one).

You can also directly write a `demi-site.yml`, of course.

(Variable expansion on-the-fly à la docker compose might be implemented later).
-->

## Global commands

### `site_config`

- Clone reference repos of all requirements defined in `demi-site.yml` 
- Locally Clone instance repos for each stack from the reference repos
- Configure instance repos using `config`  script
- Loop over instances running `docker compose` commands: convert, up -d, down


### `pydemi init`

Read information from GitLab group and projects.
Cache them locally.

## Site commands

### `pydemi site configure`

Write configuration for the site based on `demi-site.template.yml`.

## GitLab projects commands

### `pydemi projects info`

Dump info about projects.

## Git repository commands

### `pydemi repos check`

Check repositories for extra and missing files.

## Glossary

A word on nomenclature ... unfortunately some words have multiple meanings in this eco-system. Here is a short glossary

| context   | GitLab         | Docker                   | DeMi           |
| --------- | -------------- | ------------------------ | -------------- |
| "project" | GitLab Project | docker compose project   |
| "service" |                | one container definition | all containers |
| "group"   | GitLab Group   |                          |

| context | Network       | Docker         | Demi          |
| ------- | ------------- | -------------- | ------------- |
| "host"  | physical host | container host | abstract host |

Questions: Gerhard Brandt <gbrandt@cern.ch>
