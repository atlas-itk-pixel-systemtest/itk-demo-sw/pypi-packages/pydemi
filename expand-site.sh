#!/usr/bin/env bash
#
# produce the expanded site configuration

chevron  -d demi-site.data.json demi-site.template.yml | yq -y > demi-site.yml
check-jsonschema -v --schemafile demi-site.schema.json demi-site.yml

