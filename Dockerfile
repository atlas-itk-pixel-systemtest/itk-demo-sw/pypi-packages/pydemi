FROM registry.cern.ch/docker.io/library/python:3.9-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    curl \
    lsof \
    xz-utils \
    && rm -rf /var/lib/apt/lists/*

ENV PIP_ROOT_USER_ACTION=ignore
RUN pip install \
    --no-cache-dir \
    poetry==1.4.1 \
    && poetry config virtualenvs.create false

WORKDIR /usr/src/app

#┌─ most files ignored by .dockerignore!
#└──────────────────────────────────────
COPY . /usr/src/app/

RUN poetry install

# COPY root /

# ARG S6_OVERLAY_VERSION=3.1.4.1
# ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
# RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
# ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz /tmp
# RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

# ENTRYPOINT ["/init"]
